import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Asset } from '../my-assets/my-assets.component';

@Injectable()
export class MyAssetsResolver implements Resolve<any> {

  constructor(private route: ActivatedRoute, private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  resolve(): Observable<any> {
    const uriAsset = '/api/asset/Get/all';
    return this.http.get<Array<Asset>>(uriAsset, this.httpOptions).pipe(
      map( (dataAssets) => dataAssets ),
      catchError( (err) => Observable.throw(err.json().error) )
    )
  }
}