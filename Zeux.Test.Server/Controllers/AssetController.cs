using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zeux.Test.Models;
using Zeux.Test.Services;

namespace Zeux.Test.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AssetController : Controller
    {
        private readonly IAssetService _assetService;

        public AssetController(IAssetService assetService)
        {
            _assetService = assetService;
        }

        [HttpGet("[action]/{type}")]
        public async Task<IEnumerable<Asset>> Get(string type)
        {
            IEnumerable<Asset> assets;
            assets = (string.IsNullOrWhiteSpace(type) || type.ToLower() == "all")
                ? await _assetService.Get()
                : await _assetService.Get(type);
            return assets.OrderBy(x => x.Name);
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<AssetType>> GetTypes()
        {
            return (await _assetService.GetTypes()).OrderBy(x => x.Name);
        }
    }
}
